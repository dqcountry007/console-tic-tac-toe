# The print_board function takes a list of
# nine values and prints them in a "pretty"
# 3x3 board
def print_board(board):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in board:
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line
        n = n + 1
    print(output)
    print()

#1 2 3 
#4 5 6
#7 8 9
current_board = [1, 2, 3, 4, 5, 6, 7, 8, 9]
#Keeps track of who current player is, X or O
current_player = "X"

#Loop 9 times 
for turn in range(9):
    print_board(current_board)
    #Ask the player where they would like to move, their answer will be stored as a string
    response = input("Where would " + current_player + " like to move?")
    #Change the response/string into a number, so we can use it to change the list.
    #Then subtract 1 from that response/integer, since the numbers the players are entering
    #are NOT zero-based, but one-based. So when they enter 1 for their move, we want to use
    #index 0 in the current_board list. 
    space_number = int(response) - 1
    #Now that we have the response in integer form, we can change the index of the board
    #number to the space number the current player chose. 
    current_board[space_number] = current_player

    #TESTING THE TOP ROW OF THE BOARD, Spaces 1 2 3 (Indexes 0 1 2)
    #If current_board[0] value == current_board[1] value or...
        #If space 1 and space 2 on the board are both X or both O
        #AND
    #If current_board[1] value == current_board[2] value or...
        #If space 2 and space 3 on the board are both X or both O
    #Then if these two conditions are true....
        #We need to print out a statement about who won and then exit the program
    if current_board[0] == current_board[1] and current_board[1] == current_board[2]:
        print_board(current_board)
        print(current_board[0], "has won")
        exit()
    #TESTING THE SECOND ROW OF THE BOARD Spaces 4 5 6 (Indexes 3 4 5)
    if current_board[3] == current_board[4] and current_board[4] == current_board[5]:
        print_board(current_board)
        print(current_board[3], "has won")
        exit()
    #TESTING THE THIRD ROW OF THE BOARD Spaces 7 8 9 (Indexes 6 7 8)
    if current_board[6] == current_board[7] and current_board[7] == current_board[8]:
        print_board(current_board)
        print(current_board[6], "has won")
        exit()
    #TESTING THE FIRST COLUMN OF THE BOARD Spaces 1 4 7 (Indexes 0, 3, 6)
    if current_board[0] == current_board[3] and current_board[3] == current_board[6]:
        print_board(current_board)
        print(current_board[0], "has won")
        exit()
    #TESTING THE SECOND COLUMN OF THE BOARD Spaces 2 5 8 (Indexes 1 4 7)
    if current_board[1] == current_board[4] and current_board[4] == current_board[7]:
        print_board(current_board)
        print(current_board[1], "has won")
        exit()
    #TESTING THE THIRD COLUMN OF THE BOARD Spaces 3 6 9 (Indexes 2 5 8)
    if current_board[2] == current_board[5] and current_board[5] == current_board[8]:
        print_board(current_board)
        print(current_board[2], "has won")
        exit()
    #TESTING DIAGONAL WIN Spaces 1 5 9 (Indexes 0 4 8)
    if current_board[0] == current_board[4] and current_board[4] == current_board[8]:
        print_board(current_board)
        print(current_board[0], "has won")
        exit()
    #TESTING DIAGONAL WIN Spaces 3 5 7 (Indexes 2 4 6)
    if current_board[2] == current_board[4] and current_board[4] == current_board[6]:
        print_board(current_board)
        print(current_board[2], "has won")
        exit()

#Logic for changing the current player 
#If current player is X after they move...
    if current_player == "X":
        #Current player is now O
        current_player = "O"
#Else if, current player is O after they move...
    elif current_player == "O":
        #Current player is now X
        current_player = "X"





